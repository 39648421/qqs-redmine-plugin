class QqloginsController < ApplicationController
  unloadable

  def new
    redirect_uri = Setting["plugin_qqs"][:qq_redirect_uri3]
    auth_prefix = Setting["plugin_qqs"][:qq_auth_prefix]
    appid = Setting["plugin_qqs"][:qq_app_id]
    redirect_to("#{auth_prefix}#{appid}&redirect_uri=#{redirect_uri}")
  end
  
  # qq logout
  def destroy
    # clear the session's info
    session[:qq_avater_url] = nil
    session[:qq_nickname] = nil
    session[:qq_openid] = nil
    
    if (params[:code] .nil?)
      redirect_to home_url
    else
      redirect_back_or_default back_url
    end
  end
  
end
