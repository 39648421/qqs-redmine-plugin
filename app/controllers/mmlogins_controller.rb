class MmloginsController < ApplicationController
  unloadable

  # get mm auth code
  def new
    mm_redirect_uri = Setting["plugin_qqs"][:mm_redirect_uri]
    mm_auth_prefix = Setting["plugin_qqs"][:mm_auth_prefix]
    mm_appid = Setting["plugin_qqs"][:mm_app_id]
    # mm_appsecret = Setting["plugin_qqs"][:mm_app_secret]
    mm_auth_tail = Setting["plugin_qqs"][:mm_auth_tail]
    redirecturl = "#{mm_auth_prefix}#{mm_appid}&redirect_uri=http://#{mm_redirect_uri}#{mm_auth_tail}"
    redirect_to(redirecturl)
  end
  
   # mm logout
  def destroy
    # clear the session's info
    session[:qq_avater_url] = nil
    session[:mm_nickname] = nil
    session[:mm_openid] = nil
    
    if (params[:code] .nil?)
      redirect_to home_url
    else
      redirect_back_or_default back_url
    end
  end
end
