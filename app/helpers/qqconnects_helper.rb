module QqconnectsHelper
  def qq_login_session(qqconnect)
    session[:qq_nickname] = qqconnect.name
    session[:qq_openid] = qqconnect.openid
  end
  
  def mm_login_session(qqconnect)
    session[:mm_nickname] = qqconnect.mm_name
    session[:mm_openid] = qqconnect.mm_openid
  end
  
  def avater_to_session(avater_url)
    session[:qq_avater_url] = avater_url
  end
  
  def qq_logged_in?
    !session[:qq_nickname].nil?
  end
  
  def qq_binded?
    user = User.current
    if !user.nil? && !user.qqconnect.nil?
      !user.qqconnect.openid.nil?
    end
  end
  
  def mm_logged_in?
    !session[:mm_nickname].nil?
  end
  
  def mm_binded?
    user = User.current
    if !user.nil? && !user.qqconnect.nil?
      !user.qqconnect.mm_openid.nil?
    end
  end
end
