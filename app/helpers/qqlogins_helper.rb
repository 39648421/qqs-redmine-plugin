module QqloginsHelper
  # login from qq connect
  def qq_login(code, redirect_uri, appid, appkey)
    # get the qq connect info.
    qquser = Qq.new(code, redirect_uri, appid, appkey)
    if !qquser.nil?
      qq_nickname = qquser.get_user_info('https://graph.qq.com/user/get_user_info')['nickname']
      qq_avater_url = qquser.get_user_info('https://graph.qq.com/user/get_user_info')['figureurl']
      qq_openid = qquser.openid
    end
    # store the qq user's info into session.
    session[:qq_nickname] = qq_nickname
    session[:qq_avater_url] = qq_avater_url
    session[:qq_openid] = qq_openid
    qquser
  end
  
  # if qq has been binded to user. autologin.
  def qq_login_with_user(qqconn)
    user = qqconn.user
    if !user.nil?
      # save the qq session
      qq_nickname = session[:qq_nickname]
      qq_avater_url = session[:qq_avater_url]
      qq_openid = session[:qq_openid]
      logger.info "Successful authentication for '#{user.login}' from #{request.remote_ip} at #{Time.now.utc}"
      # Valid user
      self.logged_user = user
      # generate a key and set cookie if autologin
      if params[:autologin] && Setting.autologin?
        set_autologin_cookie(user)
      end
      call_hook(:controller_account_success_authentication_after, {:user => user })
      flash[:notice] = "#{user.name} #{l(:notice_qq_login)}"
      # restore the qq session
      session[:qq_nickname] = qq_nickname
      session[:qq_avater_url] = qq_avater_url
      session[:qq_openid] = qq_openid
    end
  end
end
