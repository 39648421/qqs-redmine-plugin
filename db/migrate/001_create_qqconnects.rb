class CreateQqconnects < ActiveRecord::Migration
  def change
    create_table :qqconnects do |t|
      t.string :name
      t.string :qq_no
      t.string :openid
    end
  end
end
