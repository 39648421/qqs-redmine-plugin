class AddIndexToQqconnectsOpenid < ActiveRecord::Migration
  def change
    add_index :qqconnects, :openid, unique: true
  end
end
