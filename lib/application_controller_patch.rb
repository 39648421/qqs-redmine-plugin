require_dependency 'application_controller'

module ApplicationControllerPatch
  def self.included(base) # :nodoc:
    # base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable # Send unloadable so it will not be unloaded in development
      # defind a globle var for backurl
      helper :qqconnects
    end
  end
end

Rails.configuration.to_prepare do
  ApplicationController.send(:include, ApplicationControllerPatch)
end