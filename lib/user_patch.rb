require_dependency 'user'

module UserPatch
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable # Send unloadable so it will not be unloaded in development
      has_one :qqconnect, :dependent => :destroy
    end
  end

  module InstanceMethods
    # qq login process
    def qq_logged_in(code, redirect_uri, appid, appkey)
      qquser = Qq.new(code, redirect_uri, appid,appkey)
      if !qquser.nil?
        qq_nickname = qquser.get_user_info('https://graph.qq.com/user/get_user_info')['nickname']
      end
      
      # session[:qq_nickname] = qq_nickname
      
      qquser
    end
  end
end

Rails.configuration.to_prepare do
  User.send(:include, UserPatch)
end