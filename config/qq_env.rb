module QqEnv
    # define const var
    # auth_prefix. needn't modify
    AUTH_PREFIX = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id="
    
    # the redirect_uri. need modify and support two uris.the third is reserved
    REDIRECT_URI = "http://tsman-a39648421.c9users.io/login"
    REDIRECT_URI2 = "http://tsman-a39648421.c9users.io/my/account"
    REDIRECT_URI3 = "http://tsman-a39648421.c9users.io"
    
    # qq connect app info. need modify
    APPID = "101279453"
    APPKEY = "fa4eb13f97d376eddef4494153588148"
    
    # valid_code in head of homepage for qq connect auth
    VALIDATE_CODE = "352423553516513164042163153523617"
end