# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
resources :qqconnects, only: [:create, :update, :destroy]

get 'qqlogincode' =>  'qqlogins#new'
delete 'qqlogout' =>  'qqlogins#destroy'
get 'mmlogincode' =>  'mmlogins#new'
delete 'mmlogout' =>  'mmlogins#destroy'
